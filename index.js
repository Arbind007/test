var express = require('express');
var app = express();
const port = 8000
require('dotenv').config()

app.get('/', function (req, res) {
  res.send('Hello World! ' + process.env.COLOR);
  console.log(process.env.COLOR);
});

app.listen(port, function () {
  console.log(`Example app listening on port ${port}!`);
});